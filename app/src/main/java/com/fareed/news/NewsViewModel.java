package com.fareed.news;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fareed.common.helpers.MyGson;
import com.fareed.common.helpers.ServicesHelper;
import com.fareed.common.helpers.VolleyErrorHandler;
import com.fareed.common.models.ModelNews;
import com.fareed.common.models.dto.NewsResponse;

import org.json.JSONObject;

import java.util.List;

/**
 * MVVMLiveDataViewModelsample Created by Mohammed Fareed on 12/30/2017.
 */

public class NewsViewModel extends AndroidViewModel {
    private final String TAG = NewsViewModel.class.getSimpleName() + "LOGCAT";
    MutableLiveData<List<ModelNews>> mNews;
    MutableLiveData<Boolean> mIsShowProgress;
    MutableLiveData<String> mError;

    public NewsViewModel(@NonNull Application application) {
        super(application);
        mNews = new MutableLiveData<>();
        mIsShowProgress = new MutableLiveData<>();
        mError = new MutableLiveData<>();
        loadNews();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d(TAG, "onCleared: ");

        ServicesHelper.getInstance(getApplication().getApplicationContext()).getRequestQueue().cancelAll(ServicesHelper.Tag.NEWS);
    }

    void loadNews() {
        Log.d(TAG, "loadNews: ");

        mIsShowProgress.postValue(true);
        ServicesHelper.getInstance(getApplication().getApplicationContext()).getNews(newsSuccessListener, newsErrorListener);
    }

    private Response.Listener<JSONObject> newsSuccessListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            mIsShowProgress.postValue(false);

            if (response != null) {
                NewsResponse newsResponse = MyGson.getmGson().fromJson(response.toString(), NewsResponse.class);
                if (newsResponse != null) {
                    mNews.postValue(newsResponse.getModelNews());
                } else
                    mError.postValue(null);
            } else {
                mError.postValue(null);
            }
        }
    };
    private Response.ErrorListener newsErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            mIsShowProgress.postValue(false);
            VolleyErrorHandler.getErrorMessage(getApplication().getApplicationContext(), error);

            mError.postValue(VolleyErrorHandler.getErrorMessage(getApplication().getApplicationContext(), error));
        }
    };
}
