package com.fareed.news;

import android.arch.lifecycle.LifecycleOwner;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.fareed.R;
import com.fareed.common.base.BaseActivity;

public class ActivityNews extends BaseActivity implements LifecycleOwner {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null)
            initializeFragments();

        initializeViews();
        setObservers();
    }

    private void initializeFragments() {
        getSupportFragmentManager().beginTransaction().replace(R.id.containerNews, FragmentNews.init()).commit();
    }

    @Override
    protected void initializeViews() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.news);
    }

    @Override
    protected void setObservers() {

    }
}
