package com.fareed.news;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fareed.R;
import com.fareed.common.models.ModelNews;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.fareed.common.helpers.Constants.NEWS_TYPE_ARTICLE;


/**
 * Created by Mohammed.AbdElAzeem on 12/4/2017.
 */

public class AdapterNews extends RecyclerView.Adapter<AdapterNews.NewsViewHolder> {

    private final List<ModelNews> mNewsList;

    AdapterNews(List<ModelNews> newsList) {
        if (newsList != null)
            mNewsList = newsList;
        else
            mNewsList = new ArrayList<>();
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news_row, parent, false));
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        holder.onBind(mNewsList.get(position));
    }

    @Override
    public int getItemCount() {
        return !mNewsList.isEmpty() ? mNewsList.size() : 0;
    }

    public void updateData(List<ModelNews> modelNews) {
        mNewsList.addAll(modelNews);
        notifyDataSetChanged();
    }

    //----------- ViewHolder -----------
    class NewsViewHolder extends RecyclerView.ViewHolder {
        private boolean mIsArabic = false;
        @BindView(R.id.img_news_type)
        ImageView typeImageView;
        @BindView(R.id.img_news)
        ImageView articleImageView;
        @BindView(R.id.tv_news_title)
        TextView newsTitleTextView;
        @BindView(R.id.tv_news_date)
        TextView newsDateTextView;
        @BindView(R.id.tv_news_likes)
        TextView newsLikesTextView;
        @BindView(R.id.tv_news_views)
        TextView newsViewsTextView;

        NewsViewHolder(View itemView1) {
            super(itemView1);

            ButterKnife.bind(this, itemView1);
        }

        void onBind(ModelNews modelNews) {
            String likes = itemView.getContext().getString(R.string.string_likes_s, modelNews.getLikes());
            String views = itemView.getContext().getString(R.string.string_s_views, modelNews.getNumOfViews());
            newsTitleTextView.setText(modelNews.getNewsTitle());
            newsDateTextView.setText(modelNews.getPostDate());
            newsLikesTextView.setText(likes);
            newsViewsTextView.setText(views);
            Picasso.with(itemView.getContext())
                    .load(modelNews.getImageUrl())
                    .placeholder(R.drawable.news_image_placeholder)
                    .into(articleImageView);

            if (modelNews.getNewsType().equals(NEWS_TYPE_ARTICLE))
                typeImageView.setImageResource(R.drawable.article_label);
        }
    }

    interface INewsViewHolder {
        void onBind(ModelNews modelNews);
    }
}
