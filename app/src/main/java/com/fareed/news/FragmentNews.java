package com.fareed.news;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fareed.R;
import com.fareed.common.base.BaseFragment;
import com.fareed.common.models.ModelNews;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNews extends BaseFragment {
    private NewsViewModel mNewsViewModel;
    private Context mContext;

    private RecyclerView mRecyclerView;
    private ProgressBar mProgress;
    private AdapterNews mAdapterNews;

    public FragmentNews() {
    }

    static FragmentNews init() {
        return new FragmentNews();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();

        /* ViewModels can also be used as a communication layer between different Fragments of an Activity.
         Each Fragment can acquire the ViewModel using the same key via their Activity.
         This allows communication between Fragments in a de-coupled fashion such that
         they never need to talk to the other Fragment directly.*/
        // This is achieved by swapping ".of(this)" with ".of(getActivity)".
        mNewsViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);

        initializeViews(getView());
        setObservers();
    }

    @Override
    protected void initializeViews(View v) {
        mProgress = v.findViewById(R.id.pb_news);

        mRecyclerView = v.findViewById(R.id.rv_news);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setHasFixedSize(true);
        mAdapterNews = new AdapterNews(null);
        mRecyclerView.setAdapter(mAdapterNews);
    }

    @Override
    protected void setObservers() {
        mNewsViewModel.mNews.observe(this, mNewsListObserver);

        mNewsViewModel.mIsShowProgress.observe(this, mShouldShowProgressObserver);

        mNewsViewModel.mError.observe(this, mErrorObserver);
    }

    private Observer<Boolean> mShouldShowProgressObserver = new Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean shouldShow) {
            mProgress.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
        }
    };

    private Observer<List<ModelNews>> mNewsListObserver = new Observer<List<ModelNews>>() {
        @Override
        public void onChanged(@Nullable List<ModelNews> modelNews) {
            mAdapterNews.updateData(modelNews);
        }
    };

    private Observer<String> mErrorObserver = new Observer<String>() {
        @Override
        public void onChanged(@Nullable String error) {
            Toast.makeText(mContext, error, Toast.LENGTH_LONG).show();
        }
    };
}
