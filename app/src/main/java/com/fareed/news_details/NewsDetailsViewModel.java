package com.fareed.news_details;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

/**
 * MVVMLiveDataViewModelsample Created by Mohammed Fareed on 1/6/2018.
 */

public class NewsDetailsViewModel extends AndroidViewModel {
    public NewsDetailsViewModel(@NonNull Application application) {
        super(application);

        loadArticleDetails();
    }

    private void loadArticleDetails() {

    }


}
