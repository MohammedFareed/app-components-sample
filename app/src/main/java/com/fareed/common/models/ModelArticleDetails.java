package com.fareed.common.models;

/**
 * Created by Mohammed.AbdElAzeem on 12/6/2017.
 */

public class ModelArticleDetails {
    private String NewsTitle;
    private String ItemDescription;
    private String Nid;
    private String NumofViews;
    private String Likes;
    private String PostDate;
    private String ImageUrl;
    private String NewsType;
    private String ShareURL;
    private String VideoURL;

    public String getNewsTitle() {
        return NewsTitle;
    }

    public void setNewsTitle(String NewsTitle) {
        this.NewsTitle = NewsTitle;
    }

    public String getItemDescription() {
        return ItemDescription;
    }

    public void setItemDescription(String ItemDescription) {
        this.ItemDescription = ItemDescription;
    }

    public String getNid() {
        return Nid;
    }

    public void setNid(String Nid) {
        this.Nid = Nid;
    }

    public String getNumofViews() {
        return NumofViews;
    }

    public void setNumofViews(String NumofViews) {
        this.NumofViews = NumofViews;
    }

    public String getLikes() {
        return Likes;
    }

    public void setLikes(String Likes) {
        this.Likes = Likes;
    }

    public String getPostDate() {
        return PostDate;
    }

    public void setPostDate(String PostDate) {
        this.PostDate = PostDate;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String ImageUrl) {
        this.ImageUrl = ImageUrl;
    }

    public String getNewsType() {
        return NewsType;
    }

    public void setNewsType(String NewsType) {
        this.NewsType = NewsType;
    }

    public String getShareURL() {
        return ShareURL;
    }

    public void setShareURL(String ShareURL) {
        this.ShareURL = ShareURL;
    }

    public String getVideoURL() {
        return VideoURL;
    }

    public void setVideoURL(String VideoURL) {
        this.VideoURL = VideoURL;
    }
}
