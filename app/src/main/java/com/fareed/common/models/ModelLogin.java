package com.fareed.common.models;

/**
 * Created by Mohammed.AbdElAzeem on 12/13/2017.
 */

public class ModelLogin {
    private String name;
    private String email;

    public ModelLogin(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
