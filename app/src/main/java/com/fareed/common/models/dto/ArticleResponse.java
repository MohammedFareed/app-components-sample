package com.fareed.common.models.dto;

import com.fareed.common.models.ModelArticleDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * news-task-android Created by Mohammed Fareed on 12/9/2017.
 */

public class ArticleResponse {

    @SerializedName("newsItem")
    @Expose
    private ModelArticleDetails newsItem;

    public ModelArticleDetails getNewsItem() {
        return newsItem;
    }

    public void setNewsItem(ModelArticleDetails newsItem) {
        this.newsItem = newsItem;
    }
}
