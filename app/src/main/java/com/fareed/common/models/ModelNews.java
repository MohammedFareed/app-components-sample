package com.fareed.common.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sherif.ElNady on 2/26/2017.
 */

public class ModelNews {
    @SerializedName("NewsTitle")
    @Expose
    private String newsTitle;
    @SerializedName("Nid")
    @Expose
    private String nid;
    @SerializedName("PostDate")
    @Expose
    private String postDate;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("NewsType")
    @Expose
    private String newsType;
    @SerializedName("NumofViews")
    @Expose
    private String numOfViews;
    @SerializedName("Likes")
    @Expose
    private String likes;

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }

    public String getNumOfViews() {
        return numOfViews;
    }

    public void setNumOfViews(String numOfViews) {
        this.numOfViews = numOfViews;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }
}
