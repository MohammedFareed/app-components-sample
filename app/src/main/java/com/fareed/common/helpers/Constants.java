package com.fareed.common.helpers;

/**
 * Created by Mohammed.AbdElAzeem on 12/11/2017.
 */

public class Constants {
    // drawer
    public static final int NEWS = 0;
    public static final int INNOVATION_MAP = 1;
    public static final int EVENTS_CALENDER = 2;
    public static final int LEADERSHIP_THOUGHTS = 3;
    public static final int LANGUAGE = 4;
    public static final int LOG_OUT = 5;

    // api
    public static final String NEWS_TYPE_ARTICLE = "84";

    // localization
    static final String LOCALE_ENGLISH = "en";
    static final String LOCALE_ARABIC = "ar";
    public static final String EXTRA_CHANGE_LOCALE = "Change-locale";

    static final int REQUEST_CODE_LOCATION_PERMISSION = 1995;

    public static final int RC_SIGN_IN = 1995;

    public static final String TAG_UNIVERSAL_LOG = "LOGS";

}
