package com.fareed.common.helpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.fareed.R;

public class VolleyErrorHandler {
    private static final String TAG = VolleyErrorHandler.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;

    public static String getErrorMessage(Context context, Throwable error) {
        if (context == null) return "";
        if (error == null) return context.getString(R.string.somethingWrong);

        String errorMsg = context.getString(R.string.error_unknown_error_occurred);
        try {
            if (error instanceof VolleyError) {
                if (isResponseStateProblem(error)) {
                    errorMsg = error.getMessage();
                } else if (error instanceof TimeoutError) {
                    errorMsg = context.getString(R.string.error_timeout);
                } else if (isNetworkProblem(error)) {
                    errorMsg = context.getString(R.string.error_no_internet_connection);
                } else if (isServerProblem(error)) {
                    errorMsg = context.getString(R.string.error_server_error);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "getErrorMessage: " + errorMsg);
        return errorMsg;
    }

    // --------------------------------------------

    /**
     * Determines whether the error is related to network
     *
     * @param error
     * @return
     */
    private static boolean isNetworkProblem(Object error) {
        return (error instanceof NetworkError) || (error instanceof NoConnectionError);
    }

    /**
     * Determines whether the error is related to server
     *
     * @param error
     * @return
     */
    private static boolean isServerProblem(Object error) {
        return (error instanceof ServerError) || (error instanceof AuthFailureError);
    }

    private static boolean isResponseStateProblem(Object error) {
        return (error instanceof MessageError);
    }

    // --------------->

    private static class MessageError extends VolleyError {
        private String exceptionMessage;

        public MessageError(String exceptionMessage) {
            super(exceptionMessage);
        }
    }

}
