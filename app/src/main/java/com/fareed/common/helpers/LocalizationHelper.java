package com.fareed.common.helpers;

import android.content.Context;
import android.content.res.Resources;

import java.util.Locale;

import static com.fareed.common.helpers.Constants.LOCALE_ARABIC;


/**
 * Created by Mohammed.AbdElAzeem on 12/11/2017.
 */

public class LocalizationHelper {

    public static void changeAppLanguage(Context ctx, Boolean isSwitch) {
        // Get the language to load
        String languageToLoad, oldLanguage = AppPreferences.getString(ctx, AppPreferences.LOCALE_KEY,
                AppPreferences.DEFAULT_LOCALE);
        if (isSwitch)
            if (oldLanguage.equals(LOCALE_ARABIC))
                languageToLoad = Constants.LOCALE_ENGLISH;
            else
                languageToLoad = LOCALE_ARABIC;
        else
            languageToLoad = AppPreferences.getString(ctx, AppPreferences.LOCALE_KEY,
                    AppPreferences.DEFAULT_LOCALE);

        try {
            if (languageToLoad != null && !"".equals(languageToLoad)) {
                Resources res = ctx.getApplicationContext().getResources();
                android.content.res.Configuration config = res.getConfiguration();

                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                config.setLocale(locale);
                ctx.getResources().updateConfiguration(config, ctx.getResources().getDisplayMetrics());

                // update sharedPref
                AppPreferences.setString(ctx, languageToLoad, AppPreferences.LOCALE_KEY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void setupAppLanguage(Context ctx) {
//        String languageToLoad = AppPreferences.getString(ctx, AppPreferences.LOCALE_KEY,
//                AppPreferences.DEFAULT_LOCALE);
//
//        // Get the language to load
//        try {
//            if (languageToLoad != null && !"".equals(languageToLoad)) {
//                Resources res = ctx.getApplicationContext().getResources();
//                android.content.res.Configuration config = res.getConfiguration();
//
//                Locale locale = new Locale(languageToLoad);
//                Locale.setDefault(locale);
//                config.setLocale(locale);
//                ctx.getResources().updateConfiguration(config, ctx.getResources().getDisplayMetrics());
//
//                // update sharedPref
//                AppPreferences.setString(ctx, languageToLoad, AppPreferences.LOCALE_KEY);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
