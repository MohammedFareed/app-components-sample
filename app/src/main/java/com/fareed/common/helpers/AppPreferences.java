package com.fareed.common.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.fareed.common.models.ModelLogin;

import static com.fareed.common.helpers.Constants.LOCALE_ENGLISH;

/**
 * Created by Mohammed.AbdElAzeem on 12/11/2017.
 */

public class AppPreferences {
    // shared pref
    static final String LOCALE_KEY = "locale";
    private static final String MODEL_LOGIN_KEY = "login_info";
    static final String DEFAULT_LOCALE = LOCALE_ENGLISH;

    static String getString(Context context, String key, String defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        return prefs.getString(key, defaultValue);
    }

    static void setString(Context context, String value, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        prefs.edit().putString(key, value).apply();
    }

    public static ModelLogin readModelLoginSP(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String json = prefs.getString(MODEL_LOGIN_KEY, null);
        return MyGson.getmGson().fromJson(json, ModelLogin.class);
    }

    public static void writeModelLoginToSP(Context context, ModelLogin modelLogin) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String json = MyGson.getmGson().toJson(modelLogin);
        prefs.edit().putString(MODEL_LOGIN_KEY, json).apply();
    }
}
