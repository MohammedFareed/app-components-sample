package com.fareed.common.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.location.LocationManager;

/**
 * Created by Mohammed.Fareed on 12/19/2017.
 */

public class LocationHelper {

    private static final String TAG = LocationHelper.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    /**
     * Solving the problem of dialog is showing again when OK is clicked and
     * on some devices it returns to the fragment again before going to the settings
     * So the dialog is displayed again.
     */
    public static boolean isLocationServiceDialogShown = false;
    private static final DialogInterface.OnCancelListener ON_CANCEL = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {
            isLocationServiceDialogShown = false;
        }
    };

    public static Boolean checkLocationService(Context context, DialogInterface.OnClickListener ON_ENABLE_LOCATION_BUTTON_CLICKED) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        assert locationManager != null;
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (!isLocationServiceDialogShown) {
                isLocationServiceDialogShown = true;
                DialogsHelper.showDialogWithOneButton(context,
                        "Enable Location",
                        "Please enable your location services",
                        "OK",
                        ON_ENABLE_LOCATION_BUTTON_CLICKED, ON_CANCEL);
            }
            return false;
        } else
            return true;
    }

    public static boolean isLocationServiceOn(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);;
        boolean gps_enabled = false, network_enabled = false;

        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            //do nothing...
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            //do nothing...
        }

        return gps_enabled || network_enabled;
    }


}
