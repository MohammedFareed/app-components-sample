package com.fareed.common.helpers;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Sherif.ElNady on 2/26/2017.
 */

public class MyApplication extends Application {

    private Gson mGson;

    @Override
    public void onCreate() {
        super.onCreate();
        mGson = new GsonBuilder().create();

//        Twitter.initialize(this);
    }

    public Gson getmGson() {
        return mGson;
    }
}
