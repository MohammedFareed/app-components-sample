package com.fareed.common.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;

import static com.fareed.common.helpers.Constants.REQUEST_CODE_LOCATION_PERMISSION;


/**
 * Created by Mohammed.Fareed on 12/21/2017.
 */

public class PermissionsHelper {
    private static final String TAG = PermissionsHelper.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;

    public static Boolean isPermissionGranted(Context context, String permission) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ActivityCompat.checkSelfPermission(context, permission)
                        == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermission(final Fragment fragment, String permission) {
        if (!isPermissionGranted(fragment.getContext(), permission)) {
            if (fragment.shouldShowRequestPermissionRationale(permission)) {  // show an explanation
                DialogsHelper.showDialogWithOneButton(fragment.getContext(),
                        "Location Permission is very important",
                        "This app needs the Location permission, please accept to use location functionality",
                        "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                try {
                                    fragment.requestPermissions(
                                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                            REQUEST_CODE_LOCATION_PERMISSION);
                                } catch (Exception e) {
                                    Log.e(TAG, "onClick: ", e);
                                }
                            }
                        },
                        null);
            } else { //first time normal request
                fragment.requestPermissions(new String[]{permission},
                        REQUEST_CODE_LOCATION_PERMISSION);
            }
        }
    }
}
