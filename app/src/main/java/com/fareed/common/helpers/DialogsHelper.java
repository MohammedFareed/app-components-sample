package com.fareed.common.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

/**
 * Created by Mohammed.Fareed on 12/19/2017.
 */

public class DialogsHelper {

    public static void showDialogWithOneButton(final Context context, String title, String message,
                                               String buttonText, DialogInterface.OnClickListener ON_BUTTON_CLICK, @Nullable DialogInterface.OnCancelListener ON_CANCEL) {
        if (ON_CANCEL != null)
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(buttonText, ON_BUTTON_CLICK)
                    .setOnCancelListener(ON_CANCEL)
                    .show();
        else
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(buttonText, ON_BUTTON_CLICK)
                    .show();
    }
}
