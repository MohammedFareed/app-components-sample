package com.fareed.common.helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * news-task-android-saveState Created by Mohammed Fareed on 12/23/2017.
 */

public class MyGson {
    private static Gson mGson;

    public static synchronized Gson getmGson() {
        if (mGson == null) {
            mGson = new GsonBuilder().create();
        }
        return mGson;
    }
}
