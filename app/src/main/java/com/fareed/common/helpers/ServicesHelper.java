package com.fareed.common.helpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by Sherif.ElNady on 2/26/2017.
 */
public class ServicesHelper {
    private static final String TAG = ServicesHelper.class.getSimpleName() + Constants.TAG_UNIVERSAL_LOG;
    private static ServicesHelper mInstance;
    private RequestQueue mRequestQueue;

    private final String BASE_URL = "http://egyptinnovate.com/en/api/v01/safe/";
    private final String NEWS_URL = BASE_URL + "GetNews";
    private final String NEWS_DETAILS_URL = BASE_URL + "GetNewsDetails";
    private String PARAM_NEWS_ID = "nid";

    public enum Tag {
        NEWS,
        NEWS_DETAILS
    }

    private ServicesHelper(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
    }

    public static synchronized ServicesHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ServicesHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    private <T> void addToRequestQueue(Request<T> req) {
        mRequestQueue.add(req);
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public void getNews(Response.Listener<JSONObject> successListener,
                        Response.ErrorListener errorListener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(NEWS_URL, null, successListener, errorListener);
        jsonObjectRequest.setTag(Tag.NEWS);
        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 0;
            }

            @Override
            public int getCurrentRetryCount() {
                return 3;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        //Adding our request to the queue
        addToRequestQueue(jsonObjectRequest);
    }

    public void getNewsDetails(final String Nid, Response.Listener<JSONObject> successListener,
                               Response.ErrorListener errorListener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(NEWS_DETAILS_URL + "?" + PARAM_NEWS_ID + "=" + Nid, null, successListener, errorListener);
        Log.d(TAG, "getNewsDetails: " + jsonObjectRequest.getUrl());
        jsonObjectRequest.setTag(Tag.NEWS_DETAILS);
        //Adding our request to the queue
        addToRequestQueue(jsonObjectRequest);
    }
}
