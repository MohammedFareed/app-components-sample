package com.fareed.common.di.modules;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * MVVMLiveDataViewModelsample Created by Mohammed Fareed on 1/8/2018.
 */
@Module
public class NetworkModule {
    private final String BASE_URL = "http://egyptinnovate.com/en/api/v01/safe/";
    private final String NEWS_URL = BASE_URL + "GetNews";
    private final String NEWS_DETAILS_URL = BASE_URL + "GetNewsDetails";
    private String PARAM_NEWS_ID = "nid";

    public NetworkModule() {
    }

    @Provides
    SharedPreferences providesSharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    public RequestQueue provideRequestQueue(Application application) {
        return Volley.newRequestQueue(application);
    }
}
