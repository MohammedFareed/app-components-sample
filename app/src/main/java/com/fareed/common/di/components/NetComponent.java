package com.fareed.common.di.components;

import com.fareed.common.di.modules.AppModule;
import com.fareed.common.di.modules.NetworkModule;
import com.fareed.news.FragmentNews;

import javax.inject.Singleton;

import dagger.Component;

/**
 * MVVMLiveDataViewModelsample Created by Mohammed Fareed on 1/8/2018.
 */

@Singleton
@Component(modules={AppModule.class, NetworkModule.class})
public interface NetComponent {
    void inject(FragmentNews fragmentNews);
}
